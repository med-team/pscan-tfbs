.\"                                      Hey, EMACS: -*- nroff -*-
.\" (C) Created by 2018 Steffen Moeller <moeller@debian.org> from the output of pscan -h
.\"
.TH PSCAN 1 "May  3 2018"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
pscan \- detection of transcription factor binding sites in DNA sequences
.SH SYNOPSIS
.B pscan -q multifastafile -p multifastafile
.RI [options]
.br
.B pscan -p multifastafile
.RI [options]
.br
.B pscan -q multifastafile -M matrixfile
.RI [options]

.SH DESCRIPTION
.hy
.B Pscan
inspects the upstream non-coding regions of many genes to derive subsequences
that are characteristic for the binding of proteins, i.e. transcription factors,
that control the tissue- and situation-dependent expression of a gene.
The tool is supported by the JASPAR database and other data that is downloadable
from the tool's home page.
.nh
.PP
.hy
The command line tool
.B pscan
is meant for bulk submission. The tool is  also offered with a web
interface that has all auxillary data updated.
.nh
.SH OPTIONS
.hy
.B pscan
options only have single dashes (`-') and (with notable
exceptions) followed by a single letter. Options are case-sensitive.
A summary of options is included below.
.nh
.TP
.B \-h
Show summary of options similar to this man page.
.TP
.B \-v
Show version of program.
.TP
.B \-q
.RI file
Specify the multifasta file containing the foreground sequences.
.TP
.B \-p
.RI file
Specify the multifasta file containing the background sequences.
.TP
.B \-m
.RI file
Use it if the background data are already available in a file (see -g option).
.TP
.B \-M
.RI file
Scan the foreground sequences using only the Jaspar/Transfac matrix file contained in the specified file.
.TP
.B \-l
.RI file
Use the matrices contained in that file (for matrix file format see below).
.TP
.B \-N
.RI name
Use only the matrix with that name (usable only in association with -l).
.TP
.B \-ss
Perform single strand only analysis.
.TP
.B \-rs
Perform single strand only analysis on the reverse strand.
.TP
.B \-split
.RI num1 num2
Sequences are scanned only from position num1 and for num2 nucleotides.
.TP
.B \-trashn
Discards sequences containing "N".
.TP
.B \-n
Oligos containing "N" will not be discarded. Instead a "N" will obtain an "average" score.
.TP
.B \-g
.hy
If a background sequences file is used than a file will be written containing the data calculated 
for that background sequences and the current set of matrices. 
From now on one can use that file (-m option) instead of the sequences file for faster processing.
.nh
.TP
.B \-ui file
An index of the background file will be used to avoid duplicated sequences. 
.TP
.B \-bi
Build an index of the background sequences file (to be used later with the -ui option). 
This is useful when you have duplicated sequences in your background that may introduce a bias in your results.
	
.SH NOTES
.hy
The sequences to be used with Pscan have to be promoter sequences. 
To obtain meaningful results it's critical that the background and the foreground sequences are consistent between them either in size 
and in position (with respect to the transcription start site). For optimal results the foreground set should be a subset of the background set. 
.nh
.PP
If the "-l" option is not used Pscan will try to find Jaspar/Transfac matrix files in the current folder. 
Jaspar files have ".pfm" extension while Transfac ones have ".pro" extension. 
If Jaspar matrix files are used than a file called "matrix_list.txt" must be present in the same folder. 
That file contains required info about the matrices in the ".pfm" files.

.SH EXAMPLES

.B 1)
pscan -p human_450_50.fasta -bi
.PP
.hy
This command will scan the file "human_450_50.fasta" using the matrices in the current folder. 
It is handy to use that command the first time one uses a set of matrices with a given background sequences file. 
A file called human_450_50.short_matrix will be written and it can be used from now on every time you want to use 
the same background sequences with the same set of matrices.  A file called human_450_50.index will be written too 
and it will be useful every time you will use the same background file.
.nh
.PP
.B 2)
pscan \-q human_nfy_targets.fasta \-m human_450_50.short_matrix \-ui human_450_50.index
.PP
.hy
This command will scan the file human_nfy_targets.fasta searching for over-represented binding sites (with respect 
to the preprocessed background contained in the "human_450_50.short_matrix" file) using the matrices in the current folder. 
Please note that the query file "human_nfy_targets.fasta" must be a subset of the sequences contained in the background file "human_450_50.fasta" 
in order to use the index file with the "-ui" option. This means that both the sequences and their FASTA headers used in the query file must appear
in the background file as well. Using the "-ui" option when the sequences contained in the query file are not a subset of the background file will
have undefined/unpredictable outcomes.	
The output will be a file called "human_nfy_targets.fasta.res" where you will find all the used matrices sorted by ascending P-value. 
The lower the P-value obtained by a matrix, the higher are the chances that the transcription factor associated to that matrix 
is a regulator of the input promoter sequences. 
The fields of the output are the following: "Transcription Factor Name", "Matrix ID", "Z Score", "Pvalue", "Foreground Average", "Background Average".    
.nh
.PP	
.B 3)
pscan -q human_nfy_targets.fasta -M MA0108.pfm
.PP	
.hy
This command will scan the sequences file "human_nfy_targets.fasta" using the matrix contained in "MA0108.pfm". 
The result will be written in a file called "human_nfy_targets.fasta.ris" where you will find the sequences in input 
sorted by a descending score (between 1 and 0). The higher the score, the better is the oligo found with respect to the used matrix.
The fields of the output are the following: "Sequence Header", "Score", "Position from the end of sequence", "Oligo that obtained the score", 
"Strand where the oligo was found".
.nh
.PP
.B 4)
pscan -p human_450_50.fasta -bi -l matrixfile.wil
.PP
.hy
This command is like Example #1 with the difference that the matrices set to be used is the one contained in the "matrixfile.wil" file. 
Please look at the "example_matrix_file.wil" file included in this Pscan distribution to see the correct format for matrices file.
.nh
.PP	
.B 5)
pscan -q human_nfy_targets.fasta -l matrixfile.wil -N MATRIX1
.PP
This command is like Example #3 but it will use the matrix called "MATRIX1" contained in the "matrixfile.wil" file.
.SH SEE ALSO
.BR
For info on how Pscan works pleare refer to the paper.
